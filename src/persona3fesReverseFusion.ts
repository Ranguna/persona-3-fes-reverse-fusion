import {dualFusionArcanaResults, personaList, personaNameListByArcana/**, triFusionArcanaResults*/} from "./persona3fes";
import {FusedPersonaAttributes, PersonaAttributes, PersonaName } from "./persona3fes.entity";
import {PersonaInventory} from "./persona3fesReverseFusion.entity";

const memo: Record<string, FusedPersonaAttributes> = {}

const dualFusion = (persona1: PersonaAttributes, persona2: PersonaAttributes): FusedPersonaAttributes | null => {
	if(memo[`${persona1.id}+${persona2.id}`]) return memo[`${persona1.id}+${persona2.id}`];
	if(memo[`${persona2.id}+${persona1.id}`]) return memo[`${persona2.id}+${persona1.id}`];

	const level = 1 + Math.floor((persona1.level + persona2.level) / 2);
	const dualFusionArcanaResult = dualFusionArcanaResults[`${persona1.arcana}+${persona2.arcana}`];
	const personaNameListForArcana = personaNameListByArcana[dualFusionArcanaResult];

	if(!personaNameListForArcana) return null;

	let personaIndex = personaNameListForArcana
		.findIndex(personaName =>
			personaList[personaName].level >= level
			&& !personaList[personaName].special
		)
	if(personaIndex === -1) return null;

	const possiblePersonaName = personaList[personaNameListForArcana[personaIndex]].name;
	if(
		persona1.arcana === persona2.arcana
		&& (
			possiblePersonaName === persona1.name
			|| possiblePersonaName === persona2.name
		)
	) {
		personaIndex -= 1;
	}

	memo[`${persona1.id}+${persona2.id}`] = {
		...personaList[personaNameListForArcana[personaIndex]],
		id: `${persona1.id}+${persona2.id}=${personaList[personaNameListForArcana[personaIndex]].id}`,
		persona1,
		persona2
	};
	memo[`${persona2.id}+${persona1.id}`] = memo[`${persona1.id}+${persona2.id}`];

	return memo[`${persona1.id}+${persona2.id}`];
}

const personaWithLessDepth = (persona: Exclude<PersonaInventory[keyof PersonaInventory], undefined>, personaInventory: Partial<PersonaInventory>) =>
	!personaInventory[persona.name]
		? persona
		: (
			(persona.depth ?? 0) <= (personaInventory[persona.name]!.depth ?? 0)
				? persona
				: personaInventory[persona.name]
		);

export function* getAllDualResultsAccurate(currentPersonaInventory: PersonaInventory, maxLevel = 100, maxDepth=5, depth = 0): Generator<PersonaInventory, PersonaInventory> {
	if (maxDepth === depth) return {};

	const personaNames = Object.keys(currentPersonaInventory) as PersonaName[];

	let fusedPersonas: Partial<PersonaInventory> = {};

	for(let persona1Index = 0; persona1Index < personaNames.length - 1; persona1Index++) {
		for(let persona2Index = persona1Index + 1; persona2Index < personaNames.length; persona2Index++) {
			const persona1 = currentPersonaInventory[personaNames[persona1Index]]!;
			const persona2 = currentPersonaInventory[personaNames[persona2Index]]!;
			const fusedPersona = dualFusion(
				persona1,
				persona2
			);

			if(fusedPersona && fusedPersona.level <= maxLevel && fusedPersona.level >= persona1.level && fusedPersona.level >= persona2.level && !(fusedPersona.name in currentPersonaInventory)) {
				const newPersonaInventoryAfterFusion = {
					...currentPersonaInventory,
					[fusedPersona.name]: fusedPersona,
				};
				delete newPersonaInventoryAfterFusion[persona1.name];
				delete newPersonaInventoryAfterFusion[persona2.name];

				const deeplyFusedPersonas = yield* getAllDualResultsAccurate(
					newPersonaInventoryAfterFusion,
					maxLevel,
					maxDepth,
					depth + 1
				);

				fusedPersonas = (Object.keys(deeplyFusedPersonas) as PersonaName[]).reduce(
					(accFusedPersonas, personaName) => ({
						...accFusedPersonas,
						[personaName]: personaWithLessDepth(deeplyFusedPersonas[personaName]!, accFusedPersonas)
					}),
					{
						...fusedPersonas,
						[fusedPersona.name]: {
							...fusedPersona,
							depth,
						}
					}
				)
			}
		}
	}

	yield fusedPersonas;
	return fusedPersonas;
}
