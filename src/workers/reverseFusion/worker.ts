import { WorkerPayload, WorkerResult } from './workerInterface.entity';

import { getAllDualResultsAccurate } from '../../persona3fesReverseFusion';
import { PersonaInventory } from '../../persona3fesReverseFusion.entity';

declare const self: DedicatedWorkerGlobalScope;

const tickMillis = 1 * 1000;

let personaFusionGenerator: Generator<PersonaInventory, PersonaInventory>;
let lastGeneratorResult: IteratorResult<PersonaInventory, PersonaInventory> | undefined;

setInterval(
  () => {
    const startTime = Date.now();
    while (
      Date.now() - startTime < tickMillis
			&& !lastGeneratorResult?.done
    ) {
      lastGeneratorResult = personaFusionGenerator?.next();
      if (lastGeneratorResult?.done) {
        self.postMessage({ fusionForest: lastGeneratorResult.value, done: true } as WorkerResult);
      }
    }
    if (lastGeneratorResult && !lastGeneratorResult.done) {
      self.postMessage({ fusionForest: lastGeneratorResult.value, done: false } as WorkerResult);
    }
  },
  tickMillis + 10
);

self.onmessage = function ({ data }: MessageEvent<WorkerPayload>) {
  personaFusionGenerator = getAllDualResultsAccurate(data.personaInventory, data.characterLevel, data.maxSearchDepth);
  lastGeneratorResult = undefined;
};
