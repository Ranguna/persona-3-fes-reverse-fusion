import { PersonaInventory } from '../../persona3fesReverseFusion.entity';

export interface WorkerPayload {
  personaInventory: PersonaInventory;
  characterLevel?: number;
  maxSearchDepth: number;
}

export interface WorkerResult {
  fusionForest: PersonaInventory;
  done: boolean;
}
