import {Skill} from "../persona3fes.entity";

export interface SkillSelection {
	[key: Skill["skill"]]: boolean | undefined;
}
