import {PersonaAttributes, PersonaName} from "./persona3fes.entity";

export type PersonaInventory = {[key in PersonaName]?: PersonaAttributes & {depth?: number;}};
