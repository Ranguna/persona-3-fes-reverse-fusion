export type PersonaName = 'Orpheus' | 'Slime' | 'Legion' | 'Black Frost' | 'Ose' | 'Decarabia' | 'Loki' | 'Susano-o' | 'Orpheus Telos' | 'Nekomata' | 'Jack Frost' | 'Pyro Jack' | 'Hua Po' | 'Sati' | 'Orobas' | 'Rangda' | 'Surt' | 'Apsaras' | 'Unicorn' | 'High Pixie' | 'Sarasvati' | 'Ganga' | 'Parvati' | 'Kikuri-hime' | 'Scathach' | 'Leanan Sidhe' | 'Yaksini' | 'Laksmi' | 'Hariti' | 'Gabriel' | 'Mother Harlot' | 'Skadi' | 'Alilat' | 'Forneus' | 'Oberon' | 'Take-mikazuchi' | 'King Frost' | 'Raja Naga' | 'Kingu' | 'Barong' | 'Odin' | 'Omoikane' | 'Berith' | 'Shiisaa' | 'Flauros' | 'Thoth' | 'Hokuto Seikun' | 'Daisoujou' | 'Kohryu' | 'Pixie' | 'Alp' | 'Narcissus' | 'Queen Mab' | 'Saki Mitama' | 'Titania' | 'Raphael' | 'Cybele' | 'Ara Mitama' | 'Chimera' | 'Zouchouten' | 'Ares' | 'Oumitsunu' | 'Nata Taishi' | 'Koumokuten' | 'Thor' | 'Angel' | 'Archangel' | 'Principality' | 'Power' | 'Virtue' | 'Dominion' | 'Throne' | 'Melchizedek' | 'Yomotsu Shikome' | 'Naga' | 'Lamia' | 'Mothman' | 'Taraka' | 'Kurama Tengu' | 'Nebiros' | 'Kumbhanda' | 'Arahabaki' | 'Fortuna' | 'Empusa' | 'Kusi Mitama' | 'Clotho' | 'Lachesis' | 'Atropos' | 'Norn' | 'Valkyrie' | 'Rakshasa' | 'Titan' | 'Jikokuten' | 'Hanuman' | 'Narasimha' | 'Kali' | 'Siegfried' | 'Inugami' | 'Take-minakata' | 'Orthrus' | 'Vasuki' | 'Ubelluris' | 'Hecatoncheires' | 'Hell Biker' | 'Attis' | 'Ghoul' | 'Pale Rider' | 'Loa' | 'Samael' | 'Mot' | 'Alice' | 'Thanatos' | 'Nigi Mitama' | 'Mithra' | 'Genbu' | 'Seiryuu' | 'Okuninushi' | 'Suzaku' | 'Byakko' | 'Yurlungur' | 'Lilim' | 'Vetala' | 'Incubus' | 'Succubus' | 'Pazuzu' | 'Lilith' | 'Abaddon' | 'Beelzebub' | 'Eligor' | 'Cu Chulainn' | 'Bishamonten' | 'Seiten Taisei' | 'Masakado' | 'Mara' | 'Shiva' | 'Chi You' | 'Nandi' | 'Kaiwan' | 'Ganesha' | 'Garuda' | 'Kartikeya' | 'Saturnus' | 'Helel' | 'Gurr' | 'Yamatano-orochi' | 'Girimehkala' | 'Dionysus' | 'Chernobog' | 'Seth' | 'Baal Zebul' | 'Sandalphon' | 'Yatagarasu' | 'Quetzalcoatl' | 'Jatayu' | 'Horus' | 'Suparna' | 'Vishnu' | 'Asura' | 'Anubis' | 'Trumpeter' | 'Michael' | 'Satan' | 'Lucifer' | 'Messiah' | 'Uriel' | 'Nidhoggr' | 'Ananta' | 'Atavaka' | 'Metatron';

export interface Skill {
	skill: string;
	level: number;
};

export interface PersonaAttributes {
	name: PersonaName;
	level: number;
	arcana: string;
	special?: boolean;
	max?: boolean;
	item?: boolean;
	skills?: Skill[]
	id: string;
};

export interface FusedPersonaAttributes extends PersonaAttributes {
	path?: string;
	persona1: PersonaAttributes;
	persona2: PersonaAttributes;
};

export const isFusedPersona = (persona: any): persona is FusedPersonaAttributes => typeof persona === 'object' && persona?.persona1 && persona?.persona2
